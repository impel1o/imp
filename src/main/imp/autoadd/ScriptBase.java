package autoadd;

import java.sql.*;
import java.util.Random;

/**
 * Created by d.baskakov on 06.03.2017.
 */
public class ScriptBase {
    private static final String user = "root";
    private static final String password = "root";
    private static final String url = "jdbc:mysql://localhost:3306/imp?useUnicode=true&useSSL=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    private static Connection con;

    public static void main(String[] args) {

        String insert1000 = "INSERT INTO users VALUES(?,?,?,?,?)";
        String testName = "TestName";

        PreparedStatement preparedStatement = null;
        Random r = new Random();
        try {
            con = DriverManager.getConnection(url, user, password);

            for (int i = 1; i < 21; i++) {
                preparedStatement = con.prepareStatement(insert1000);
                preparedStatement.setInt(1, i);
                preparedStatement.setString(2, testName + i);
                preparedStatement.setInt(3, (int) (Math.random() * 88));
                preparedStatement.setBoolean(4,r.nextBoolean());
                preparedStatement.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
                preparedStatement.execute();
            }


        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException se) {
            }
            try {
                preparedStatement.close();
            } catch (SQLException se) {
            }
        }

        System.out.println("DONE!");
    }
}
