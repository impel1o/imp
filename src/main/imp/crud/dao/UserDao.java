package crud.dao;

import crud.model.User;

import java.util.List;

/**
 * Created by d.baskakov on 06.03.2017.
 */
public interface UserDao {
    public void addUser(User user);
    public void updateUser(User user);
    public void removeUser(int id);
    public User getUserById(int id);
    public User getUserById1(int id);
    public List<User> listUsers();
    public List<User> findUsersByPage(int pageid,int total);
}
