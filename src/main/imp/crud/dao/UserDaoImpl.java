package crud.dao;

import crud.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by d.baskakov on 06.03.2017.
 */

@Repository
public class UserDaoImpl implements UserDao {

    private static final Logger logger= LoggerFactory.getLogger(UserDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void addUser(User user) {
        Session session=this.sessionFactory.getCurrentSession();
        session.persist(user);
        logger.info("user added "+ user);

    }

    @Override
    public void updateUser(User user) {
        Session session=this.sessionFactory.getCurrentSession();
        session.update(user);
        logger.info("user updated "+ user);
    }

    @Override
    public void removeUser(int id) {
        Session session=this.sessionFactory.getCurrentSession();
        User user=(User)session.load(User.class,new Integer(id));

        if (user!=null)
        {
            session.delete(user);
            logger.info("user delete "+ user);
        }
    }

    @Override
    public User getUserById(int id) {
        Session session=this.sessionFactory.getCurrentSession();
        User user=(User)session.load(User.class,new Integer(id));
        logger.info("user load "+ user);
        return user;
    }

    @Override
    public User getUserById1(int id) {
        Session session=this.sessionFactory.getCurrentSession();
        User user=(User)session.get(User.class,new Integer(id));
        logger.info("user get "+ user);
        return user;
    }

    @Override
    @SuppressWarnings("uncheked")
    public List<User> listUsers() {
        Session session=this.sessionFactory.getCurrentSession();
        List<User> userList=session.createQuery("from User").list();
        for (User user:userList)
        {
            logger.info("User list "+user);
        }
        return userList;
    }

    @Override
    public List<User> findUsersByPage(int pageid, int total) {
        Session session = sessionFactory.getCurrentSession();
        String sql="select * from users limit "+(pageid-1)+","+total;
        List<User> users = session.createSQLQuery(sql).addEntity(User.class).list();
        return users;
    }
}
